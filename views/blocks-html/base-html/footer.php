<!--Подвал-->
<footer class="footer">

    <div class="container footer-container">

        <div class="footer-nav">

            <div class="footer_nav__row">
                <a href="#" class="footer-nav__item">О сервисе</a>
                <a href="#" class="footer-nav__item">Вопрос-ответ</a>
            </div>

            <div class="footer_nav__row">
                <a href="#" class="footer-nav__item">Правила</a>
                <a href="#" class="footer-nav__item red">lorem</a>
            </div>

            <div class="footer_nav__row">
                <a href="#" class="footer-nav__item">Партнерам</a>
                <a href="#" class="footer-nav__item red">lorem</a>
            </div>

        </div>

        <div class="footer-right">

            <div class="footer__social-links">
                <a href="#"><i class="icon-facebook-rect-1"></i></a>
                <a href="#"><i class="icon-instagram-4"></i></a>
                <a href="#"><i class="icon-telegram"></i></a>
            </div>

            <div class="footer__copyright">
                <span>&copy; Ideas For Travel, <?= date("Y") ?></span>
            </div>

        </div>

    </div>

</footer>