<!--Центральная часть-->
<div class="center">

    <!--навигация для индекс-->
    <nav class="container index-nav">
        <ul class="index-nav__menu">
            <li><a href="catalog.php">КАТАЛОГ</a>
            <li><a href="organizer.php">КАБИНЕТ ПАРТНЕРА</a></li>
            <li><a href="tour.php">КАРТОЧКА ТУРА</a></li>
        </ul>
    </nav>

    <section class="main-image"></section>

    <section class="container how-it-works">

        <div class="how-it-works__box">
            <div class="how-it-works__where-to-go"></div>
            <h3>Не знаете куда отправиться в отпуск?</h3>
            <p>Раздел <a href="#">«Идеи путешествий»</a> поможет найти варианты отдыха по вашим увлечениям,
                по ожидаемым впечатлениям, по интересным местам или по желаемому типу отдыха.</p>
        </div>

        <div class="how-it-works__box">
            <div class="how-it-works__how-to-choose"></div>
            <h3>Как выбрать лучшее предложение?</h3>
            <p>Сохраняйте понравившиеся варианты туров. В два клика сравнивайте предложения на сайте по
                различным критериям и выбриайте только самые интересные программы. Общайтесь с
                организаторами и гидам в чате. </p>
        </div>

        <div class="how-it-works__box">
            <div class="how-it-works__subscribe"></div>
            <h3>Не нашли интересных предложений?</h3>
            <p>Подпишитесь на новые предложения по интересующим вас направлениям. Пришлите нам свои идеи
                путешествий и мы займемся поиском. Как только на сайте появятся туры по вашим интересам, вы
                будете первым, кто об этом узнает.</p>
        </div>
    </section>

    <!--Коллекции-->
    <section class="container">
        <!--<div class="collections">
                <div class="collection-box-A">
                    <div class="collection-image-1"><a href="#">Куда поехать без визы<i class="icofont-simple-right"></i></a></div>
                    <div class="collection-image-2"><a href="#">На майские праздники<i class="icofont-simple-right"></i></a></div>
                 </div>

                <div class="collection-box-B">
                    <div class="collection-image-3"><a href="#">По чудесам света<i class="icofont-simple-right"></i></a></div>
                    <div class="collection-image-4"><a href="#">Незнакомая Европа<i class="icofont-simple-right"></i></a></div>
                </div>
        </div>-->
    </section>

    <!--Популярные туры-->
    <section class="container">
        <div class="popular-tours"></div>
    </section>

</div>