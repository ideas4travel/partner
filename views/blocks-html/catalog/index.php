<!-- .center - блок с контентом страницы \\ .catalog - указывает на старницу каталога -->
<div class="center">
    <!--блок с заданной шириной для контента-->
    <div class="container catalog-container">

        <!-- блоки отвечающие за фильтры каталога-->
        <section class="filter">

            <!-- все блоки основного фильтра (слева на десктопе) -->
            <div class="main-filter">

                <div class="mob-view-filter-head">

                    <div class="mob-btn-1">

                        <span>ФИЛЬТР (9) </span>

                        <i class="icon-sliders"></i>

                    </div>

                </div>

                <div class="dt-view-filter-head">

                    <span>ГИБКИЙ ПОИСК</span>

                </div>

                <div class="mob-view-filter-options">

                    <div class="mob-filter-settings">

                        <div class="mob-btn-1">

                            <span class="filter-clean">СБРОСИТЬ</span>

                            <i class="icofont-bin"></i>

                        </div>

                        <div class="mob-btn-1">

                            <span class="mob-filter-close">ЗАКРЫТЬ</span>

                            <i class="icofont-close mob-filter-close"></i>

                        </div>
                    </div>

                    <div class="mob-applied-filters">

                        <div class="mob-btn-1">

                            <span>Выбранные <span> (9)</span></span>

                            <i class="icon-angle-double-right hide-element"></i>
                            <i class="icon-angle-double-left"></i>

                        </div>

                        <div class="mob-applied-filters-result hide-element">
                            <div class="tags">
                                <div class="filter-tag"><span>КРИТЕРИИ ТУРА</span></div>
                                <div class="filter-tag"><span>ЦЕНА</span></div>
                                <div class="filter-tag"><span>КОМФОРТ</span></div>
                                <div class="filter-tag"><span>ДИНАМИКА</span></div>
                                <div class="filter-tag"><span>НАПРАВЛЕНИЕ</span></div>
                                <div class="filter-tag"><span>ПЕРИОД</span></div>
                                <div class="filter-tag"><span>ИНТЕРЕСЫ</span></div>
                                <div class="filter-tag"><span>МЕСТА</span></div>
                                <div class="filter-tag"><span>ТИП ТУРА</span></div>
                            </div>
                        </div>

                    </div>

                    <div class="mob-view-show-result">

                        <span>ПОКАЗАТЬ (25)</span>

                    </div>

                </div>

                <!-- меню и настройки основного фильтра -->
                <div class="main-filter-options">

                    <form action="">

                        <div class="filter-box">

                            <div class="filter-heading">

                                <div class="filter-icon">

                                    <img src="../../images/icons/filter-criteries.png" alt="icon">

                                </div>

                                <div class="filter-name">

                                    <span>КРИТЕРИИ ТУРА</span>

                                    <i class="icon-down-open-big"></i>
                                    <i class="icon-up-open-big"></i>

                                </div>

                            </div>

                            <div class="filter-params hide-element">

                                <span>параметры</span>

                            </div>

                        </div>

                        <div class="filter-box">

                            <div class="filter-heading">

                                <div class="filter-icon">

                                    <img src="../../images/icons/filter-price.png" alt="icon">

                                </div>

                                <div class="filter-name">

                                    <span>ЦЕНА</span>

                                    <i class="icon-down-open-big"></i>
                                    <i class="icon-up-open-big"></i>

                                </div>

                            </div>

                            <div class="filter-params hide-element">

                                <span>параметры</span>

                            </div>

                        </div>

                        <div class="filter-box">

                            <div class="filter-heading">

                                <div class="filter-icon">

                                    <img src="../../images/icons/filter-comfort.png" alt="icon">

                                </div>

                                <div class="filter-name">

                                    <span>КОМФОРТ</span>

                                    <i class="icon-down-open-big"></i>
                                    <i class="icon-up-open-big"></i>

                                </div>

                            </div>

                            <div class="filter-params hide-element">

                                <span>параметры</span>

                            </div>

                        </div>

                        <div class="filter-box">

                            <div class="filter-heading">

                                <div class="filter-icon">

                                    <img src="../../images/icons/filter-dynamics.png" alt="icon">

                                </div>

                                <div class="filter-name">

                                    <span>ДИНАМИКА</span>

                                    <i class="icon-down-open-big"></i>
                                    <i class="icon-up-open-big"></i>

                                </div>

                            </div>

                            <div class="filter-params hide-element">

                                <span>параметры</span>

                            </div>

                        </div>

                        <div class="filter-box">

                            <div class="filter-heading">

                                <div class="filter-icon">

                                    <img src="../../images/icons/filter-direction.png" alt="icon">

                                </div>

                                <div class="filter-name">

                                    <span>НАПРАВЛЕНИЕ</span>

                                    <i class="icon-down-open-big"></i>
                                    <i class="icon-up-open-big"></i>

                                </div>

                            </div>

                            <div class="filter-params hide-element">

                                <span>параметры</span>

                            </div>

                        </div>

                        <div class="filter-box">

                            <div class="filter-heading">

                                <div class="filter-icon">

                                    <img src="../../images/icons/filter-dates.png" alt="icon">

                                </div>

                                <div class="filter-name">

                                    <span>ПЕРИОД</span>

                                    <i class="icon-down-open-big"></i>
                                    <i class="icon-up-open-big"></i>

                                </div>

                            </div>

                            <div class="filter-params hide-element">

                                <span>параметры</span>

                            </div>

                        </div>

                        <div class="filter-box">

                            <div class="filter-heading">

                                <div class="filter-icon">

                                    <img src="../../images/icons/filter-interests.png" alt="icon">

                                </div>

                                <div class="filter-name">

                                    <span>ИНТЕРЕСЫ</span>

                                    <i class="icon-down-open-big"></i>
                                    <i class="icon-up-open-big"></i>

                                </div>

                            </div>

                            <div class="filter-params hide-element">

                                <span>параметры</span>

                            </div>

                        </div>

                        <div class="filter-box">

                            <div class="filter-heading">

                                <div class="filter-icon">

                                    <img src="../../images/icons/filter-places.png" alt="icon">

                                </div>

                                <div class="filter-name">

                                    <span>МЕСТА</span>

                                    <i class="icon-down-open-big"></i>
                                    <i class="icon-up-open-big"></i>

                                </div>

                            </div>

                            <div class="filter-params hide-element">

                                <span>параметры</span>

                            </div>

                        </div>

                        <div class="filter-box">

                            <div class="filter-heading">

                                <div class="filter-icon">

                                    <img src="../../images/icons/filter-tour_type.png" alt="icon">

                                </div>

                                <div class="filter-name">

                                    <span>ТИП ТУРА</span>

                                    <i class="icon-down-open-big"></i>
                                    <i class="icon-up-open-big"></i>

                                </div>

                            </div>

                            <div class="filter-params hide-element">

                                <span>параметры</span>

                            </div>

                        </div>

                    </form>

                </div>

            </div>

            <!-- быстрый фильтр (выпадающее меню справа) -->
            <div class="quick-filter">

                <form action="#" method="get">

                    <select class="quick-filter-options" name="quick-filter-options" id="quick-filter">

                        <option selected class="quick-filter-item">Популярные</option>
                        <option class="quick-filter-item">Ближайшие</option>
                        <option class="quick-filter-item">Новинки</option>
                        <option class="quick-filter-item">По оценке</option>
                        <option class="quick-filter-item">По цене вниз</option>
                        <option class="quick-filter-item">По цене вверх</option>

                    </select>

                </form>

            </div>

        </section>

        <!-- каталог туров  -->
        <section class="tours">

            <!-- витрина для отображения всех туров  -->
            <div class="tour-window">

                <!-- бокс для отображения тура  -->
                <div class="tour-box">

                    <div class="tour-image">

                        <div class="to-favorite">
                            <i class="icon-heart-empty"></i>
                            <i class="icon-heart hide-element"></i>
                        </div>

                        <div class="tour-info">
                            <span>14 дней</span>
                            <span> &bull; </span>
                            <span>78 500 &#8381; </span>
                        </div>

                        <img src="../../images/tours/tour1.jpg" alt="tour1">
                    </div>

                    <div class="tour-definition">

                        <h2>На море всей семьей: Сказочная встреча с Черногорией</h2>
                        <span>...</span>

                    </div>

                </div>

                <div class="tour-box">

                    <div class="tour-image">

                        <div class="to-favorite">
                            <i class="icon-heart-empty"></i>
                            <i class="icon-heart hide-element"></i>
                        </div>

                        <div class="tour-info">
                            <span>21 день</span>
                            <span> &bull; </span>
                            <span>1 080 &#36; </span>
                        </div>

                        <img src="../../images/tours/tour2.jpg" alt="tour2">
                    </div>

                    <div class="tour-definition">
                        <h2>Путешествие в зазеркалье: Чили, Боливия и Перу</h2>
                        <span>...</span>
                    </div>
                </div>


                <div class="program-box">

                    <div class="program__image">

                        <img src="../../images/tours/tour_main_img_00001.jpg" alt="tour-image">

                        <div class="image__bottom-left-tag">
                            <span>12 дней</span>
                            <span> &bull; </span>
                            <span>1 080 &#36; </span>
                        </div>

                    </div>

                    <div class="program__info">

                        <div class="group">
                            <span class="program__country">Черногория</span>
                            <span class="program__name">На море всей семьей: Сказочная встреча с Черногорией</span>
                        </div>

                        <i class="icon-dot-3"></i>

                    </div>

                </div>

                <div class="program-box">

                    <div class="program__image">

                        <img src="../../images/tours/tour_main_img_00010.jpg" alt="tour-image">

                        <div class="image__bottom-left-tag">
                            <span>14 дней</span>
                        </div>

                    </div>

                    <div class="program__info">

                        <div class="group">
                            <span class="program__country">Россия, Непал</span>
                            <span class="program__name">Треккинг к Эвересту | Южный базовый лагерь + озера Гокио</span>
                        </div>

                        <i class="icon-dot-3"></i>

                    </div>

                </div>

                <div class="program-box">

                    <div class="program__image">

                        <img src="../../images/tours/tour_main_img_00004.jpg" alt="tour-image">

                        <div class="image__bottom-left-tag">
                            <span>6 дней</span>
                        </div>

                    </div>

                    <div class="program__info">

                        <div class="group">
                            <span class="program__country">ИРАН</span>
                            <span class="program__name">Путешествие в Иран. Восхождение на Демавенд (5671м). Вулкан, маковые поля, древние столицы Персии.</span>
                        </div>

                        <i class="icon-dot-3"></i>

                    </div>

                </div>

                <div class="program-box">

                    <div class="program__image">

                        <img src="../../images/tours/tour_main_img_00007.jpg" alt="tour-image">

                        <div class="image__bottom-left-tag">
                            <span>17 дней</span>
                        </div>

                    </div>

                    <div class="program__info">

                        <div class="group">
                            <span class="program__country">Австралия, Новая Зеландия</span>
                            <span class="program__name">Новая Зеландия: 'Русские Хоббиты'</span>
                        </div>

                        <i class="icon-dot-3"></i>

                    </div>

                </div>

            </div>

        </section>

    </div>

</div>