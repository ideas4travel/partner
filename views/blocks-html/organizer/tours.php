<!-- <div class="center"> -->

<section class="container organizer__tours">

    <div class="back-n-wrap" data-page="tours">
        <span><a href="organizer.php">туры на сайте</a></span>
    </div>

    <div class="tours-table">

        <div class="tours-table__head">
            <div class="tours-table__data">Старт</div>
            <div class="tours-table__program">Программа</div>
            <div class="tours-table__price">Цена</div>
            <div class="tours-table__extra-costs">Экстра</div>
            <div class="tours-table__guide">Гид</div>
            <div class="tours-table__status">Статус</div>
            <div class="tours-table__edit">...</div>
        </div>

        <div class="tours-table__row">
            <div class="tours-table__data">29.05.19</div>
            <div class="tours-table__program">
                <img class="tours-table__img" src="../../images/tours/tour_main_img_00010.jpg" alt="tour-img">
                <span class="tours-table__program-name">Треккинг к Эвересту | Южный базовый лагерь + озера Гокио</span>
            </div>
            <div class="tours-table__price">670 $</div>
            <div class="tours-table__extra-costs">320 $</div>
            <div class="tours-table__guide">Dmitry Kravchenko</div>
            <div class="tours-table__status">
                        <span class="status__icon">
                            <img src="../../images/icons/icon_status-guaranteed.svg" alt="status-icon">
                        </span>
            </div>

            <div class="tours-table__edit">

                <i class="icon-pencil-1"></i>

                <div class="edit__menu hide-element">
                    <span>Редактировать</span>
                    <span class="red">Удалить</span>
                </div>

            </div>

        </div>

        <div class="tours-table__row">
            <div class="tours-table__data">08.06.19</div>
            <div class="tours-table__program">
                <img class="tours-table__img" src="../../images/tours/tour_main_img_00004.jpg" alt="tour-img">
                <span class="tours-table__program-name">Путешествие в Иран. Восхождение на Демавенд (5671м). Вулкан, маковые поля, древние столицы Персии.</span>
            </div>
            <div class="tours-table__price">1100 $</div>
            <div class="tours-table__extra-costs">-</div>
            <div class="tours-table__guide">-</div>
            <div class="tours-table__status">
                        <span class="status__icon">
                            <img src="../../images/icons/icon_status-guaranteed.svg" alt="status-icon">
                        </span>
            </div>

            <div class="tours-table__edit">

                <i class="icon-pencil-1"></i>

                <div class="edit__menu hide-element">
                    <span>Редактировать</span>
                    <span class="red">Удалить</span>
                </div>

            </div>

        </div>

        <div class="tours-table__row">
            <div class="tours-table__data">16.06.19</div>
            <div class="tours-table__program">
                <img class="tours-table__img" src="../../images/tours/tour_main_img_00001.jpg" alt="tour-img">
                <span class="tours-table__program-name">На море всей семьей: Сказочная встреча с Черногорией</span>
            </div>
            <div class="tours-table__price">980 $</div>
            <div class="tours-table__extra-costs">-</div>
            <div class="tours-table__guide">Мусияченко Нина Анатольевна</div>
            <div class="tours-table__status">
                        <span class="status__icon">
                            <img src="../../images/icons/icon_status-guaranteed.svg" alt="status-icon">
                        </span>
            </div>

            <div class="tours-table__edit">

                <i class="icon-pencil-1"></i>

                <div class="edit__menu hide-element">
                    <span>Редактировать</span>
                    <span class="red">Удалить</span>
                </div>

            </div>

        </div>

        <div class="tours-table__row">
            <div class="tours-table__data">14.09.19</div>
            <div class="tours-table__program">
                <img class="tours-table__img" src="../../images/tours/tour_main_img_00010.jpg" alt="tour-img">
                <span class="tours-table__program-name">Треккинг к Эвересту | Южный базовый лагерь + озера Гокио</span>
            </div>
            <div class="tours-table__price">650 $</div>
            <div class="tours-table__extra-costs">320 $</div>
            <div class="tours-table__guide">Dmitry Kravchenko</div>
            <div class="tours-table__status">
                        <span class="status__icon">
                            <img src="../../images/icons/icon_status-recruitment.svg" alt="status-icon">
                        </span>
            </div>

            <div class="tours-table__edit">

                <i class="icon-pencil-1"></i>

                <div class="edit__menu hide-element">
                    <span>Редактировать</span>
                    <span class="red">Удалить</span>
                </div>

            </div>

        </div>

        <div class="tours-table__row">
            <div class="tours-table__data">15.01.20</div>
            <div class="tours-table__program">
                <img class="tours-table__img" src="../../images/tours/tour_main_img_00007.jpg" alt="tour-img">
                <span class="tours-table__program-name">Новая Зеландия: 'Русские Хоббиты'</span>
            </div>
            <div class="tours-table__price">3333 $</div>
            <div class="tours-table__extra-costs">500 $</div>
            <div class="tours-table__guide">-</div>
            <div class="tours-table__status">
                        <span class="status__icon">
                            <img src="../../images/icons/icon_status-recruitment.svg" alt="status-icon">
                        </span>
            </div>

            <div class="tours-table__edit">

                <i class="icon-pencil-1"></i>

                <div class="edit__menu hide-element">
                    <span>Редактировать</span>
                    <span class="red">Удалить</span>
                </div>

            </div>

        </div>

    </div>

    <div class="tours__bottom">

        <div class="btn-blue">
            <span>Опубликовать</span>
        </div>

        <div class="btn-grey">
            <span>Архив</span>
        </div>

    </div>

</section>

</div>