<!-- <div class="center"> -->

    <section id="organizer-profile" class="container organizer__profile hide-element">

        <div id="profile-wrap" class="back-n-wrap" data-page="profile">
            <span>профль партнера</span>
        </div>

        <div class="organizer__information">

            <div class="main__group">

                <div class="organizer__main">

                    <div class="organizer__avatar">

                        <img src="../../images/test/organizer_company.jpg" alt="organizer_avatar">

                        <div class="avatar__edit fade hide-element">
                            <span class="close-pop-up"></span>
                            <form action="#" method="post">
                                <label for="partner-avatar" class="info-field__head">Аватар профиля</label>
                                <input type="file" id="partner-avatar" accept=".jpg, .jpeg, .png">
                                <button type="submit" class="btn-blue"><span>Загрузить</span></button>
                            </form>
                        </div>

                    </div>

                    <div class="organizer__registered">

                        <span class="info-field__head">на сайте</span>
                        <span class="blue">1 год и 12 месяцев</span>

                    </div>

                </div>

                <div class="organizer__contacts">

                    <div class="organizer__info-field">

                        <span class="info-field__head">Организатор</span>

                        <div class="help hover">
                            <div class="help-pop-up right fade hide-element">
                                <span class="close-pop-up"></span>
                                <p>При необходимости изменить регистрационные данные следуйте инструкции в базе знаний:
                                    <a class="link" href="#">Личный кабинет партнера</a></p>
                            </div>
                        </div>

                        <span class="info-field__content">YourOwnAdventure</span>

                    </div>

                    <div class="organizer__info-field">

                        <span class="info-field__head">Контактное лицо</span>

                        <span class="info-field__content">Константин Константинопольский</span>

                    </div>

                    <div class="organizer__info-field">

                        <span class="info-field__head">Email</span>

                        <span class="info-field__content link">info@yowad.ru</span>

                    </div>

                    <div class="organizer__info-field">

                        <span class="info-field__head">Телефон</span>

                        <span class="info-field__content">+7-916-111-14-51</span>

                    </div>

                </div>

            </div>

            <!-- ОРГАНИЗАЦИЯ: отображение блока после проверки реквизитов профиля модератором -->

            <div class="organizer__personal-data">

                <h2 class="block-title">ОРГАНИЗАЦИЯ</h2>

                <div class="organizer__info-field">

                    <span class="info-field__head">ДЕЯТЕЛЬНОСТЬ</span>

                    <span class="info-field__content">Компания</span>

                </div>

                <div class="organizer__info-field">

                    <span class="info-field__head">ПРАВОВАЯ&nbsp;ФОРМА</span>

                    <span class="info-field__content">Индивидуальный предприниматель</span>

                </div>

                <div class="organizer__info-field">

                    <span class="info-field__head">НАЗВАНИЕ</span>

                    <span class="info-field__content">Каримбаев Артур Камалиддинович</span>

                </div>

                <div class="organizer__info-field">

                    <span class="info-field__head">ИНН</span>

                    <span class="info-field__content">502604209612</span>

                </div>

                <div class="organizer__info-field">

                    <span class="info-field__head">Юридический&nbsp;адрес</span>

                    <span class="info-field__content">140083, Московская область, г. Лыткарино, 5 микрорайон, квартал 2, 11-13</span>

                </div>

                <div class="organizer__info-field">

                    <span class="info-field__head">Фактический&nbsp;адрес</span>

                    <span class="info-field__content">140083, Московская область, г. Лыткарино, 5 микрорайон, квартал 2, 11-13</span>

                </div>

                <div class="organizer__info-field">

                    <span class="info-field__head">ДОП.&nbsp;ТЕЛЕФОН</span>

                    <span class="info-field__content">8-499-350-38-45</span>

                </div>

                <div class="organizer__info-field">

                    <span class="info-field__head">ДОП.&nbsp;EMAIL</span>

                    <span class="info-field__content link">yowad@yandex.ru</span>

                </div>

            </div>

            <!-- КОНТАКТЫ: отображение блока после сохранения пользователем информации (при редактировании можно только добавлять данные и нельзя удалять информацию после проверки профиля) -->

            <div class="organizer__links">

                <div class="edit-menu">

                    <div class="menu-list fade hide-element">
                        <span>Редактировать</span>
                    </div>

                </div>

                <h2 class="block-title">Контакты</h2>

                <div class="links__group">

                    <div class="organizer__info-field">

                        <span class="info-field__icon website"></span>

                        <span class="info-field__content link">www.yowad.ru</span>

                    </div>

                    <div class="organizer__info-field">

                        <span class="info-field__icon facebook"></span>

                        <span class="info-field__content link">www.facebook.com/yourownadventure</span>

                    </div>

                    <div class="organizer__info-field">

                        <span class="info-field__icon instagram"></span>

                        <span class="info-field__content link">www.instagram.com/yourownadventure</span>

                    </div>

                    <div class="organizer__info-field">

                        <span class="info-field__icon vkontacte"></span>

                        <span class="info-field__content link">www.vk.com/yowad</span>

                    </div>

                    <div class="organizer__info-field">

                        <span class="info-field__icon youtube empty-icon"></span>

                        <span class="info-field__content unfilled">нет данных</span>

                    </div>

                </div>

                <div class="links__group">

                    <div class="organizer__info-field">

                        <span class="info-field__icon telegram"></span>

                        <span class="info-field__content link">@yowad</span>

                    </div>

                    <div class="organizer__info-field">

                        <span class="info-field__icon whatsapp"></span>

                        <span class="info-field__content link">+7-916-11-11-451</span>

                    </div>

                    <div class="organizer__info-field">

                        <span class="info-field__icon viber"></span>

                        <span class="info-field__content link">+7-916-11-11-451</span>

                    </div>

                    <div class="organizer__info-field">

                        <span class="info-field__icon skype empty-icon"></span>

                        <span class="info-field__content unfilled">нет данных</span>

                    </div>

                    <div class="organizer__info-field">

                        <span class="info-field__icon phone"></span>

                        <span class="info-field__content">
                            +7 499 350 38 45 (Россия)<br>
                            +375 29 115 55 15  (Беларусь)
                        </span>

                    </div>

                </div>

            </div>

            <!-- ОБ ОРГАНИЗАТОРЕ (блок всегда доступен для редактирования без обращения к модератору) -->

            <div class="organizer__about">

                <div class="edit-menu">

                    <div class="menu-list fade hide-element">
                        <span>Редактировать</span>
                    </div>

                </div>

                <h2 class="block-title">Об&nbsp;организаторе</h2>

                <div class="organizer__info-field">

                    <div class="info-field__content">
                        <!-- Ограничить кол-во символов до 1000 знаков с пробелами -->
                        <p>Девиз Yowad - «Создай свое собственное приключение».</p>
                        <p>Мы создаем приключения для тех, кто уже не кайфует от пляжно-отельного отдыха, он создан для тех, кто хочет видеть и познавать мир, несмотря на возможные трудности, которые встретятся на пути.</p>
                        <p>Единственное, трудности мы называем словом приключение. Так вот путешествие не будет запоминающимся, если в нем не будет приключений.</p>
                        <p>Каких только приключений не было в моих путешествиях, главное всегда философски относиться к ним, не паниковать и не конфликтовать.</p>
                        <p>Именно готовностью к приключениям путешественник отличается от обыкновенного туриста. И мы стараемся учить этому в наших путешествиях.</p>
                    </div>

                </div>

            </div>

        </div>

    </section>

<!-- закрывающий тег для <div class="center"> / начало в nav.php -->
</div>
