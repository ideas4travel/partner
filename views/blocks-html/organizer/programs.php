<!-- <div class="center"> -->

<section class="container organizer__programs">

    <div class="back-n-wrap" data-page="programs">
        <span><a href="organizer.php">программы туров</a></span>
    </div>

    <div class="programs__block">

        <div class="program-box">

            <div class="program__image">

                <img src="../../images/tours/tour_main_img_00001.jpg" alt="tour-image">

                <div class="edit-menu">

                    <div class="menu-list right fade hide-element">
                        <span>Предпросмотр</span>
                        <span>Редактировать</span>
                        <span>Дублировать</span>
                        <span class="grey">Опубликовать</span>
                        <span class="red">Удалить</span>
                    </div>

                </div>

                <div class="image__bottom-left-tag">
                    <span>12 дней</span>
                </div>

            </div>

            <div class="program__info">

                <div class="program__country">Черногория</div>

                <div class="program__name">На море всей семьей: Сказочная встреча с Черногорией</div>

                <div class="program__stats">

                    <span class="program__estimation">7,6<span class="small-txt">10</span></span>

                    <span class="program__comfort">3<span class="small-txt">3</span></span>

                    <span class="program__dynamics">1<span class="small-txt">3</span></span>

                </div>
            </div>

        </div>

        <div class="program-box">

            <div class="program__image">

                <img src="../../images/tours/tour_main_img_00010.jpg" alt="tour-image">

                <div class="edit-menu">
                   
                    <div class="menu-list right fade hide-element">
                        <span>Предпросмотр</span>
                        <span>Редактировать</span>
                        <span>Дублировать</span>
                        <span class="grey">Опубликовать</span>
                        <span class="red">Удалить</span>
                    </div>

                </div>

                <div class="image__bottom-left-tag">
                    <span>14 дней</span>
                </div>

            </div>

            <div class="program__info">

                <div class="program__country">Россия, Непал</div>

                <div class="program__name">Треккинг к Эвересту | Южный базовый лагерь + озера Гокио</div>

                <div class="program__stats">

                    <span class="program__estimation">0<span class="small-txt">10</span></span>

                    <span class="program__comfort">2<span class="small-txt">3</span></span>

                    <span class="program__dynamics">3<span class="small-txt">3</span></span>

                </div>

            </div>

        </div>

        <div class="program-box">

            <div class="program__image">

                <img src="../../images/tours/tour_main_img_00004.jpg" alt="tour-image">

                <div class="edit-menu">
                  
                    <div class="menu-list right fade hide-element">
                        <span>Предпросмотр</span>
                        <span>Редактировать</span>
                        <span>Дублировать</span>
                        <span class="grey">Опубликовать</span>
                        <span class="red">Удалить</span>
                    </div>

                </div>

                <div class="image__bottom-left-tag">
                    <span>6 дней</span>
                </div>

            </div>

            <div class="program__info">

                <div class="program__country">ИРАН</div>

                <div class="program__name">Путешествие в Иран. Восхождение на Демавенд (5671м).
                    Вулкан, маковые поля, древние столицы Персии.
                </div>

                <div class="program__stats">

                    <span class="program__estimation">8,0<span class="small-txt">10</span></span>

                    <span class="program__comfort">2<span class="small-txt">3</span></span>

                    <span class="program__dynamics">3<span class="small-txt">3</span></span>

                </div>

            </div>

        </div>

        <div class="program-box">

            <div class="program__image">

                <img src="../../images/tours/tour_main_img_00007.jpg" alt="tour-image">

                <div class="edit-menu">
                   
                    <div class="menu-list right fade hide-element">
                        <span>Предпросмотр</span>
                        <span>Редактировать</span>
                        <span>Дублировать</span>
                        <span class="grey">Опубликовать</span>
                        <span class="red">Удалить</span>
                    </div>

                </div>

                <div class="image__bottom-left-tag">
                    <span>17 дней</span>
                </div>

            </div>

            <div class="program__info">

                <div class="program__country">Австралия, Новая Зеландия</div>

                <div class="program__name">Новая Зеландия: 'Русские Хоббиты'</div>

                <div class="program__stats">

                    <span class="program__estimation">0<span class="small-txt">10</span></span>

                    <span class="program__comfort">2<span class="small-txt">3</span></span>

                    <span class="program__dynamics">2<span class="small-txt">3</span></span>

                </div>

            </div>

        </div>

    </div>

    <div class="programs__bottom">

        <!--<div class="btn-blue">
            <span>Опубликовать</span>
        </div>-->

        <div class="btn-orange">
            <a href="/programs/create"><span>Создать</span></a>
        </div>

        <div class="btn-grey">
            <span>Архив</span>
        </div>

    </div>

</section>
<!-- закрывающий тег к <div class="center"> (начало в nav.php) -->
</div>