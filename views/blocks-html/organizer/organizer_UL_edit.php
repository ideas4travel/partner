<!-- <div class="center"> -->

<section id="organizer-profile" class="container organizer__profile hide-element">

    <div id="profile-wrap" class="back-n-wrap" data-page="profile">
        <span>профль партнера</span>
    </div>

    <div class="organizer__information">

        <div class="main__group">

            <div class="organizer__main">

                <div class="organizer__avatar">

                    <!-- АВАТАР по умолчанию для всех пользователей 100px*100px -->
                    <img src="../../images/icons/avatar.png" alt="organizer_avatar">

                    <div class="avatar__edit fade hide-element">
                        <form action="#" method="post">
                            <span class="close-pop-up"></span>
                            <label for="partner-avatar" class="info-field__head"><span>Аватар профиля</span>
                                <input type="file" id="partner-avatar" accept=".jpg, .jpeg, .png">
                            </label>
                            <button type="submit" class="btn-blue"><span>Загрузить</span></button>
                        </form>
                    </div>

                </div>

                <div class="organizer__registered">

                    <span class="info-field__head">на сайте</span>
                    <span class="blue">1 год и 12 месяцев</span>

                </div>

            </div>

            <div class="organizer__contacts">

                <div class="organizer__info-field">

                    <span class="info-field__head">Организатор</span>

                    <div class="help hover">
                        <div class="help-pop-up right fade hide-element">
                            <span class="close-pop-up"></span>
                            <p>При необходимости изменить регистрационные данные следуйте инструкции в базе знаний:</p>
                            <a class="link" href="#">Личный кабинет партнера</a>
                        </div>
                    </div>

                    <span class="info-field__content">YourOwnAdventure</span>

                </div>

                <div class="organizer__info-field">

                    <span class="info-field__head">Контактное лицо</span>

                    <span class="info-field__content">Константин Константинопольский</span>

                </div>

                <div class="organizer__info-field">

                    <span class="info-field__head">Email</span>

                    <span class="info-field__content link">info@yowad.ru</span>

                </div>

                <div class="organizer__info-field">

                    <span class="info-field__head">Телефон</span>

                    <span class="info-field__content">+7-916-111-14-51</span>

                </div>

            </div>

        </div>

        <div class="organizer__activate">

            <div>
                <div class="help hover">
                    <div class="help-pop-up top fade hide-element">
                        <span class="close-pop-up"></span>
                        <p>Чтобы публиковать предложения на сайте необходимо пройти проверку СБ.</p>
                        <p>Внесите необходимую информацию и нажмите кнопку "Подтвердить профиль".</p>
                        <p>Подробности здесь: <a class="link" href="#">Личный кабинет партнера</a></p>
                    </div>
                </div>

                <button class="btn-grey" type="submit">
                    <span>Подтвердить профиль</span>
                </button>

            </div>

        </div>

        <!-- ИНФОРМАЦИЯ_ОРГАНИЗАЦИЯ: отображение блока после сохранения пользователем информации о реквизитах до момента отправки на проверку модератору -->

        <div class="organizer__personal-data">

            <div class="hover edit-menu">

                <div class="menu-list fade hide-element">
                    <span><a href="#">Редактировать</a></span>
                </div>

            </div>

            <h2 class="block-title">ОРГАНИЗАЦИЯ</h2>

            <div class="organizer__info-field">

                <span class="info-field__head">ДЕЯТЕЛЬНОСТЬ</span>

                <span class="info-field__content">Компания</span>

            </div>

            <div class="organizer__info-field">

                <span class="info-field__head">ПРАВОВАЯ&nbsp;ФОРМА</span>

                <span class="info-field__content">Индивидуальный предприниматель</span>

            </div>

            <div class="organizer__info-field">

                <span class="info-field__head">НАЗВАНИЕ</span>

                <span class="info-field__content">Каримбаев Артур Камалиддинович</span>

            </div>

            <div class="organizer__info-field">

                <span class="info-field__head">ИНН</span>

                <span class="info-field__content unfilled">нет данных</span>

            </div>

            <div class="organizer__info-field">

                <span class="info-field__head">Юридический&nbsp;адрес</span>

                <span class="info-field__content unfilled">нет данных</span>

            </div>

            <div class="organizer__info-field">

                <span class="info-field__head">Фактический&nbsp;адрес</span>

                <span class="info-field__content unfilled">нет данных</span>

            </div>

            <div class="organizer__info-field">

                <span class="info-field__head">ДОП.&nbsp;ТЕЛЕФОН</span>

                <span class="info-field__content unfilled">нет данных</span>

            </div>

            <div class="organizer__info-field">

                <span class="info-field__head">ДОП.&nbsp;EMAIL</span>

                <span class="info-field__content unfilled">нет данных</span>

            </div>

        </div>

        <!-- КОНТАКТЫ: отображение блока после сохранения пользователем информации (при редактировании можно только добавлять данные и нельзя удалять информацию после проверки профиля) -->


        <form action="#" method="post" class="organizer__links">

            <h2 class="block-title">Контакты</h2>

            <div class="links__group">

                <div class="organizer__info-field">

                    <label for="website" class="info-field__icon website empty-icon"></label>

                    <input id="website" type="text" class="info-field__content" placeholder="адрес сайта">

                </div>

                <div class="organizer__info-field">

                    <label for="facebook" class="info-field__icon facebook empty-icon"></label>

                    <input id="facebook" type="text" class="info-field__content" placeholder="страница в facebook">

                </div>

                <div class="organizer__info-field">

                    <label for="instagram" class="info-field__icon instagram empty-icon"></label>

                    <input id="instagram" type="text" class="info-field__content"
                           placeholder="страница в instagram">

                </div>

                <div class="organizer__info-field">

                    <label class="info-field__icon vkontacte empty-icon"></label>

                    <input id="vkontacte" type="text" class="info-field__content"
                           placeholder="страница в vkontacte">

                </div>

                <div class="organizer__info-field">

                    <label class="info-field__icon youtube empty-icon"></label>

                    <input id="youtube" type="text" class="info-field__content" placeholder="страница в youtube">

                </div>

            </div>

            <div class="links__group">

                <div class="organizer__info-field">

                    <label for="telegram" class="info-field__icon telegram empty-icon"></label>

                    <input id="telegram" type="text" class="info-field__content" placeholder="контакт в telegram">

                </div>

                <div class="organizer__info-field">

                    <label for="whatsapp" class="info-field__icon whatsapp empty-icon"></label>

                    <input id="whatsapp" type="text" class="info-field__content" placeholder="контакт в whatsapp">

                </div>

                <div class="organizer__info-field">

                    <label for="viber" class="info-field__icon viber empty-icon"></label>

                    <input id="viber" type="text" class="info-field__content" placeholder="контакт в viber">

                </div>

                <div class="organizer__info-field">

                    <label for="skype" class="info-field__icon skype empty-icon"></label>

                    <input id="skype" type="text" class="info-field__content" placeholder="контакт в skype">

                </div>

                <div class="organizer__info-field">

                    <label for="phone" class="info-field__icon phone empty-icon"></label>

                    <input id="phone" type="tel" class="info-field__content" placeholder="телефон для заявок">

                </div>

            </div>

            <div class="organizer__button">
                <button type="submit" class="btn-blue">
                    <span>Сохранить</span>
                </button>
            </div>

        </form>

        <!-- ОБ ОРГАНИЗАТОРЕ: после внесения изменений (блок всегда доступен для редактирования без обращения к модератору) -->

        <div class="organizer__about">

            <h2 class="block-title">Об&nbsp;организаторе</h2>

            <form action="#" method="post">

                <div class="organizer__info-field">
                    <textarea id="organizer-about" rows="6" maxlength="1000"
                              placeholder="краткое описание вашей деятельности"></textarea>
                </div>

                <div class="organizer__button">
                    <button type="submit" class="btn-blue">
                        <span>Сохранить</span>
                    </button>
                </div>

            </form>

        </div>

    </div>

</section>

<!-- закрывающий тег для <div class="center"> / начало в nav.php -->
</div>