<!-- <div class="center"> -->

<section id="organizer-profile" class="container organizer__profile hide-element">

    <div id="profile-wrap" class="back-n-wrap" data-page="profile">
        <span>профль партнера</span>
    </div>

    <div class="organizer__information">

        <div class="main__group">

            <div class="organizer__main">

                <div class="organizer__avatar">

                    <img src="../../images/test/guide0002.jpg" alt="organizer_avatar">

                    <div class="avatar__edit fade hide-element">
                        <form action="#" method="post">
                            <label for="partner-avatar" class="info-field__head">Аватар профиля</label>
                            <input type="file" id="partner-avatar" accept=".jpg, .jpeg, .png">
                            <button type="submit" class="btn-blue"><span>Загрузить</span></button>
                        </form>

                    </div>

                </div>

                <div class="organizer__registered">

                    <span class="info-field__head">на сайте</span>
                    <span class="blue">1 неделя</span>

                </div>

            </div>

            <div class="organizer__contacts">

                <div class="organizer__info-field">

                    <span class="info-field__head">Организатор</span>

                    <div class="help hover">
                        <div class="help-pop-up right fade hide-element">
                            <span class="close-pop-up"></span>
                            <p>При необходимости изменить регистрационные данные следуйте инструкции в базе знаний:
                                <a class="link" href="#">Личный кабинет партнера</a></p>
                        </div>
                    </div>

                    <span class="info-field__content">Приключения с Артуром Ивановым</span>

                </div>

                <div class="organizer__info-field">

                    <span class="info-field__head">Контактное лицо</span>

                    <span class="info-field__content">Артур Иванов</span>

                </div>

                <div class="organizer__info-field">

                    <span class="info-field__head">Email</span>

                    <span class="info-field__content link">aivanov@gmail.com</span>

                </div>

                <div class="organizer__info-field">

                    <span class="info-field__head">Телефон</span>

                    <span class="info-field__content">+7-903-963-25-75</span>

                </div>

            </div>

        </div>

        <!-- ИНФОРМАЦИЯ_ЧАСТНЫЙ ГИД: отображение блока после сохранения пользователем информации о реквизитах до момента отправки на проверку модератору -->

        <div class="organizer__personal-data">

            <h2 class="block-title">Частный гид</h2>

            <div class="organizer__info-field">

                <span class="info-field__head">Дата&nbsp;рождения</span>

                <span class="info-field__content">14.09.1983</span>

            </div>

            <div class="organizer__info-field">

                <span class="info-field__head">РЕГИСТРАЦИЯ</span>

                <span class="info-field__content">Россия, 143906, МО, Балашиха, мкрн Изумрудный, дом 7, кв. 100</span>

            </div>

            <div class="organizer__info-field">

                <span class="info-field__head">АДРЕС&nbsp;ПРОЖИВАНИЯ</span>

                <span class="info-field__content">Россия, 143906, МО, Балашиха, мкрн Изумрудный, дом 7, кв. 100</span>

            </div>

            <div class="organizer__info-field">
                <span class="info-field__head">стаж&nbsp;в&nbsp;туризме</span>
                <span class="info-field__content">10 лет</span>
            </div>

            <div class="organizer__info-field">
                <span class="info-field__head">опыт&nbsp;в&nbsp;организации&nbsp;туров</span>
                <span class="info-field__content">2 года</span>
            </div>

            <div class="organizer__info-field">
                <span class="info-field__head">проведено&nbsp;туров</span>
                <span class="info-field__content">3 группы</span>
            </div>

        </div>

        <!-- КОНТАКТЫ: отображение блока после сохранения пользователем информации (при редактировании можно только добавлять данные и нельзя удалять информацию после проверки профиля) -->

        <div class="organizer__links">

            <div class="hover edit-menu">
                <div class="menu-list fade hide-element">
                    <span>Редактировать</span>
                </div>
            </div>

            <h2 class="block-title">Контакты</h2>

            <div class="links__group">

                <div class="organizer__info-field">

                    <span class="info-field__icon website"></span>

                    <span class="info-field__content link">www.yowad.ru</span>

                </div>

                <div class="organizer__info-field">

                    <span class="info-field__icon facebook"></span>

                    <span class="info-field__content link">www.facebook.com/yourownadventure</span>

                </div>

                <div class="organizer__info-field">

                    <span class="info-field__icon instagram"></span>

                    <span class="info-field__content link">www.instagram.com/yourownadventure</span>

                </div>

                <div class="organizer__info-field">

                    <span class="info-field__icon vkontacte"></span>

                    <span class="info-field__content link">www.vk.com/yowad</span>

                </div>

                <div class="organizer__info-field">

                    <span class="info-field__icon youtube empty-icon"></span>

                    <span class="info-field__content unfilled">нет данных</span>

                </div>

            </div>

            <div class="links__group">

                <div class="organizer__info-field">

                    <span class="info-field__icon telegram"></span>

                    <span class="info-field__content link">@yowad</span>

                </div>

                <div class="organizer__info-field">

                    <span class="info-field__icon whatsapp"></span>

                    <span class="info-field__content link">+7-916-11-11-451</span>

                </div>

                <div class="organizer__info-field">

                    <span class="info-field__icon viber"></span>

                    <span class="info-field__content link">+7-916-11-11-451</span>

                </div>

                <div class="organizer__info-field">

                    <span class="info-field__icon skype empty-icon"></span>

                    <span class="info-field__content unfilled">нет данных</span>

                </div>

                <div class="organizer__info-field">

                    <span class="info-field__icon phone"></span>

                    <span class="info-field__content">
                            +7-903-963-25-75
                        </span>

                </div>

            </div>

        </div>

        <!-- ОБ ОРГАНИЗАТОРЕ: после внесения изменений (блок всегда доступен для редактирования без обращения к модератору) -->

        <div class="organizer__about">

            <div class="hover edit-menu">
                <div class="menu-list fade hide-element">
                    <span>Редактировать</span>
                </div>
            </div>

            <h2 class="block-title">Об&nbsp;организаторе</h2>

            <div class="organizer__info-field">

                <div class="info-field__content">
                    <!-- Ограничить кол-во символов до 1000 знаков с пробелами -->
                    <p>Эксперт по погружениям в Индию и Непал!</p>
                    <p>Путешествие — это тройной восторг: предвкушение, осуществление и воспоминание. А если речь идет
                        об Индии - то все эмоции можно смело умножать на десять!</p>
                    <p>Вас ожидает настоящее Погружение в ИНДИЮ!</p>
                </div>

            </div>

        </div>

    </div>

</section>

<!-- закрывающий тег для <div class="center"> / начало в nav.php -->
</div>