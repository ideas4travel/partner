<!-- открывающий тег \ закрывающий ьег находится на другом лэйауте в папке www\blocks-html\organizer-->
<div class="center">

    <nav id="nav-organizer" class="nav-organizer hide-element">

        <ul class="container nav-organizer__menu">

            <li id='chat-head' class="nav-organizer__link">
                <a href="#">
                    <span class="nav-organizer__left">Сообщения</span>

                    <span class="nav-organizer__right">
                    <span class="nav-organizer__unread">0</span>
                    <span class="small-txt">10</span>
                    <i class="icon-right-open-big"></i>
                </span>
                </a>
            </li>

            <li id="requests-head" class="nav-organizer__link">
                <a href="/requests">
                    <span class="nav-organizer__left">Заявки</span>

                    <span class="nav-organizer__right">
                        <span class="nav-organizer__unread">+1</span>
                        <span class="small-txt">6</span>
                        <i class="icon-right-open-big"></i>
                    </span>
                </a>
            </li>

            <li id="tours-head" class="nav-organizer__link">
                <a href="/tours">
                    <span class="nav-organizer__left">Туры</span>

                    <span class="nav-organizer__right">
                        <i class="icon-right-open-big"></i>
                    </span>
                </a>
            </li>

            <li id="programs-head" class="nav-organizer__link">
                <a href="/programs">
                    <span class="nav-organizer__left">Программы</span>

                    <span class="nav-organizer__right">
                        <i class="icon-right-open-big"></i>
                    </span>
                </a>
            </li>

            <li id="guides-head" class="nav-organizer__link">
                <a href="/guides">
                    <span class="nav-organizer__left">Гиды</span>

                    <span class="nav-organizer__right">
                        <i class="icon-right-open-big"></i>
                    </span>
                </a>
            </li>

            <li id="profile-head" class="nav-organizer__link">
                <a href="/organizer">
                <!-- <a href="organizer.php"> -->
                <!-- <a name="organizer-profile"> -->
                    <span class="nav-organizer__left">Профиль</span>

                    <span class="nav-organizer__right">
                    <i class="icon-right-open-big"></i>
                    </span>
                </a>
            </li>

        </ul>

    </nav>