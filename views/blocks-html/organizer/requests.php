<!-- <div class="center"> -->

    <section class="container organizer__requests">

        <div class="back-n-wrap" data-page="requests">
            <span><a href="organizer.php">заявки на бронирования</a></span>
        </div>

        <div class="requests-table">

            <div class="requests-table__head">
                <div class="requests-table__data">Дата</div>
                <div class="requests-table__program">Программа</div>
                <div class="requests-table__period">Период</div>
                <div class="requests-table__amount">Кол-во</div>
                <div class="requests-table__price">Цена</div>
                <div class="requests-table__status">Статус</div>
            </div>

            <div class="requests-table__row">
                <div class="requests-table__data">23.01.19</div>
                <div class="requests-table__program">
                    <img class="requests-table__img" src="../../images/tours/tour_main_img_00001.jpg" alt="tour-img">
                    <span class="requests-table__program-name">На море всей семьей: Сказочная встреча с Черногорией</span>
                </div>
                <div class="requests-table__period">16.06.19<br>28.06.19</div>
                <div class="requests-table__amount">3 чел</div>
                <div class="requests-table__price">980 $</div>
                <div class="requests-table__status">
                    <i class="icon-mail-4"></i>
                    <span class="requests-table__status-comment">Новый</span></div>
            </div>

            <div class="requests-table__row">
                <div class="requests-table__data">08.01.19</div>
                <div class="requests-table__program">
                    <img class="requests-table__img" src="../../images/tours/tour_main_img_00004.jpg" alt="tour-img">
                    <span class="requests-table__program-name">Путешествие в Иран. Восхождение на Демавенд (5671м). Вулкан, маковые поля, древние столицы Персии.</span>
                </div>
                <div class="requests-table__period">08.06.19<br>14.06.19</div>
                <div class="requests-table__amount">1 чел</div>
                <div class="requests-table__price">1100 $</div>
                <div class="requests-table__status">
                    <i class="icon-dot-3"></i>
                    <span class="requests-table__status-comment">В обработке</span></div>
            </div>

            <div class="requests-table__row">
                <div class="requests-table__data">18.12.18</div>
                <div class="requests-table__program">
                    <img class="requests-table__img" src="../../images/tours/tour_main_img_00007.jpg" alt="tour-img">
                    <span class="requests-table__program-name">Новая Зеландия: 'Русские Хоббиты'</span>
                </div>
                <div class="requests-table__period">15.01.20<br>01.02.20</div>
                <div class="requests-table__amount">2 чел</div>
                <div class="requests-table__price">3333 $</div>
                <div class="requests-table__status">
                    <i class="icon-cancel-3"></i>
                    <span class="requests-table__status-comment">Не подтвержден</span></div>
            </div>

            <div class="requests-table__row">
                <div class="requests-table__data">02.11.18</div>
                <div class="requests-table__program">
                    <img class="requests-table__img" src="../../images/tours/tour_main_img_00010.jpg" alt="tour-img">
                    <span class="requests-table__program-name">Треккинг к Эвересту | Южный базовый лагерь + озера Гокио</span>
                </div>
                <div class="requests-table__period">14.09.19<br>28.09.19</div>
                <div class="requests-table__amount">1 чел</div>
                <div class="requests-table__price">650 $</div>
                <div class="requests-table__status">
                    <i class="icon-money-1"></i>
                    <span class="requests-table__status-comment">Подтвержден</span></div>
            </div>

            <div class="requests-table__row">
                <div class="requests-table__data">02.11.18</div>
                <div class="requests-table__program">
                    <img class="requests-table__img" src="../../images/tours/tour_main_img_00010.jpg" alt="tour-img">
                    <span class="requests-table__program-name">Треккинг к Эвересту | Южный базовый лагерь + озера Гокио</span>
                </div>
                <div class="requests-table__period">14.09.19<br>28.09.19</div>
                <div class="requests-table__amount">2 чел</div>
                <div class="requests-table__price">650 $</div>
                <div class="requests-table__status">
                    <i class="icon-cancel-3"></i>
                    <span class="requests-table__status-comment">Отменен</span></div>
            </div>

            <div class="requests-table__row">
                <div class="requests-table__data">29.10.18</div>
                <div class="requests-table__program">
                    <img class="requests-table__img" src="../../images/tours/tour_main_img_00001.jpg" alt="tour-img">
                    <span class="requests-table__program-name">На море всей семьей: Сказочная встреча с Черногорией</span>
                </div>
                <div class="requests-table__period">26.06.18<br>04.07.18</div>
                <div class="requests-table__amount">2 чел</div>
                <div class="requests-table__price">1280 $</div>
                <div class="requests-table__status">
                    <i class="icon-ok-3"></i>
                    <span class="requests-table__status-comment">Состоялся</span></div>
            </div>

        </div>

        <div class="requests__bottom">
            <div class="btn-grey">
                <span>Архив</span>
            </div>
        </div>

    </section>
<!-- закрывающий тег к <div class="center"> (начало в nav.php) -->
</div>