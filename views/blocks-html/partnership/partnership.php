<div class="center">

    <div class="container">

        <section class="partnership">

            <div class="btn-blue">

                <a href="partnership-login.php"><span>Кабинет партнера</span></a>

            </div>

            <h2 class="block-title">Партнерам</h2>

            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error illum obcaecati repudiandae. Ipsum nam
                odit
                officiis porro quam quasi quibusdam. A aspernatur consectetur earum eligendi eveniet laboriosam maxime
                provident sint!</p>

            <a href="partnership-offer.php">Договор-оферта для партнеров</a>

        </section>

        <div style="padding: 15px">
            <h3>Тестовые ссылки</h3>
            <p><a href="partnership-activation.php">АКТИВАЦИЯ</a></p>
            <p><a href="service-page.php">СЛУЖЕБНОЕ СООБЩЕНИЕ</a></p>
            <p><a href="organizer.php">КАБИНЕТ ПАРТНЕРА</a></p>
        </div>

    </div>

</div>