<?php

// проверяем сделан ли запрос при помощи ajax, если нет, то прекращаем работу
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) && empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
    exit;
}

header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', false);
header('Pragma: no-cache');

use app\base\App;
use app\services\Auth;
/**
 * @var Auth $auth
 * @property Auth $auth;
 */

$auth = App::get()->auth;
echo "I write and testing Controller ajax. Id пользователя: " . $auth->getUserId();
